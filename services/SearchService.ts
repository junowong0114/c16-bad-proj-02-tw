import { Knex } from "knex";

export class SearchService {
    constructor(private knex: Knex) { };

    async searchBook(keyword: string) {
        return await this.knex.select(this.knex.raw(
            'books.id,title,Json_agg(DISTINCT authors.name) as authors_name ,isbn13,image_path,image_url, Json_agg(DISTINCT categories.name) as categories_name'))
            .from('books')
            .leftJoin('books_authors',{'books.id':'books_authors.book_id'})
            .leftJoin('authors',{'books_authors.author_id':'authors.id'})
            .leftJoin('books_categories',{'books.id':'books_categories.book_id'})
            .leftJoin('categories',{'books_categories.category_id':'categories.id'})
            .leftJoin('formats',{'books.format_id':'formats.id'})
            .where('title','ilike',`%${keyword}%`)
            .orWhere('authors.name','ilike',`%${keyword}%`)
            .orWhere('description','like',`%${keyword}%`)
            .groupBy('books.id','title','isbn13','image_path','image_url',)
            .limit(5)
    }

    async getBook(id: number) {
        return await this.knex.select(this.knex.raw(
            'books.id,title,isbn13,image_path,image_url,description, JSON_agg(DISTINCT authors.name) AS authors_name, JSON_agg(DISTINCT categories.name) AS categories_name'
        )).from('books')
            .leftJoin('books_authors', { 'books.id': 'books_authors.book_id' })
            .leftJoin('authors', { 'books_authors.author_id': 'authors.id' })
            .leftJoin('books_categories', { 'books.id': 'books_categories.book_id' })
            .leftJoin('categories', { 'books_categories.category_id': 'categories.id' })
            .where('books.id', '=', id)
            .groupBy('books.id', 'title', 'isbn13', 'image_path', 'image_url', 'description')
    }
}