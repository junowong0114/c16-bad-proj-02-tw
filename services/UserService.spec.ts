import Knex from "knex";
import { UserService } from "./UserService";


const knexConfigs = require('../knexfile');
const knexConfig = knexConfigs['test'];
const knex = Knex(knexConfig);


describe('UserService',()=>{
    let userService:UserService;

    beforeEach(async()=>{
        await knex('administrators').del();
        userService = new UserService(knex);

        await knex("administrators").insert([
            { id: 1, username: "admin@gmail.com",password:"$2a$10$4Q0BpntO7g4cFY/h3wRykOiNw4gnuHkkkbKIXdFGUkNf.HsB7poU6" } // password:123
        ])
    });


    it('should get user', async()=>{
        const user = await userService.getUser('admin@gmail.com');
        expect(user).toMatchObject([
            { id: 1, username: "admin@gmail.com",password:"$2a$10$4Q0BpntO7g4cFY/h3wRykOiNw4gnuHkkkbKIXdFGUkNf.HsB7poU6" } 
        ])
    })

    it('should update password',async()=>{
        const id = 1;
        const newPassword = 'a hashed password';

        await userService.updatePassword(id,newPassword);
        const newUserInfo = await userService.getUser('admin@gmail.com');

        expect(newUserInfo).toMatchObject([
            { id: 1, username: "admin@gmail.com",password:"a hashed password" } 
        ])
    })

    afterEach(async()=>{
        await knex('administrators').del();
    })

})