import { Knex } from "knex";

export class CategoriesService {
    constructor(private knex: Knex) { };

    async getCategories() {
        return await this.knex.select('*').from('categories')
    }
}