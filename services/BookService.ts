import { Knex } from "knex";

export class BookService {
    constructor(private knex: Knex) { };

    async addBook(title: string, author: string, isbn: string, description: string, categories: string[], image: string) {
        const testId = await this.knex('books').max('id');
        const bookId = await this.knex("books").insert({
            id: testId[0].max + 1,
            title: title,
            isbn13: isbn,
            description: description,
            image_url: image
        }).returning('id');

        if (categories.length > 0) {
          let categoriesQuery = this.knex('categories').select('id')
          for (let category of categories) {
            categoriesQuery = categoriesQuery.orWhere({id: category})
          }
          const categoryIds = (await categoriesQuery)
          for (let row of categoryIds) {
            const id = row.id
            await this.knex('books_categories').insert({
              book_id: bookId[0],
              category_id: id
          })
          }
        }

        let authorIdQueryResult = await this.knex('authors').select('id').where('name', author)
        let authorId
        if (authorIdQueryResult[0] === undefined) {
            authorId = (await this.knex('authors').insert({
                name: author
            }).returning('id'))[0];
        } else {
          authorId = authorIdQueryResult[0].id
        }

        await this.knex('books_authors').insert({
            book_id: parseInt(bookId[0]),
            author_id: authorId
        })

        return { success: true };
    }
}