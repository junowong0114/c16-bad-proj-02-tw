import Knex from "knex";
import { SearchService } from "./SearchService";

const knexConfigs = require('../knexfile');
const knexConfig = knexConfigs['test'];
const knex = Knex(knexConfig);

describe('SearchService',()=>{
    let searchService:SearchService;

    beforeAll(async()=>{
        await knex('books_authors').del();
        await knex('books_categories').del();
        await knex('categories').del();
        await knex('authors').del();
        await knex('books').del();
        await knex('formats').del();
        
        searchService = new SearchService(knex)


        await knex.insert([
            {id:1,name:'Stephen King'},
            {id:2,name:'Rowling'}
        ]).into('authors')

        await knex.insert([
            {id:1,name:'Accounting'},
            {id:2,name:'Adventure'}
        ]).into('categories')

        await knex.insert([
            {id:1,name:'CD'},
            {id:2,name:'books'}
        ]).into('formats')

        await knex.insert([
            {
                id:1,
                title:'Stranger Filling',
                description:'any',
                isbn13:1409173359,
                image_path:'full/3/a/4/3a444f0d8cb2b4ac55938ea31b5f76f151b4b5a3.jpg',
                format_id:2,
                image_url:'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9781/4091/9781409173359.jpg',
            }
        ]).into('books')

        await knex.insert([
            {book_id:1,author_id:1},
            {book_id:1,author_id:2}
        ]).into('books_authors')

        await knex.insert([
            {book_id:1,category_id:2}
        ]).into('books_categories')


    })

    it('should search books', async ()=>{
        const books = await searchService.searchBook('stranger');

        expect(books).toMatchObject([{
            id: 1,
            title: 'Stranger Filling',
            isbn13: "1409173359",
            image_path: 'full/3/a/4/3a444f0d8cb2b4ac55938ea31b5f76f151b4b5a3.jpg',
            image_url: 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9781/4091/9781409173359.jpg',
            authors_name: ['Rowling','Stephen King'],
            categories_name: ['Adventure']
        }])

    })

    it('should get a book information',async ()=>{
        const book = await searchService.getBook(1);

        expect(book).toMatchObject([{
            id: 1,
            title: 'Stranger Filling',
            isbn13: "1409173359",
            image_path: 'full/3/a/4/3a444f0d8cb2b4ac55938ea31b5f76f151b4b5a3.jpg',
            image_url: 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9781/4091/9781409173359.jpg',
            authors_name: ['Rowling','Stephen King'],
            categories_name: ['Adventure'],
            description: 'any',
        }])

    })



    afterAll(async()=>{
        await knex('books_authors').del();
        await knex('books_categories').del();
        await knex('categories').del();
        await knex('authors').del();
        await knex('books').del();
        await knex('formats').del();
    })

})