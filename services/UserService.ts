import { Knex } from "knex";

export class UserService{
    constructor(private knex:Knex){};

    async getUser(username:string){
        return this.knex.select('*').from('administrators').where('username',username);
    }

    async updatePassword(id:number,password:string){
        return this.knex('administrators').where('id','=',id).update('password',password);
    }


}