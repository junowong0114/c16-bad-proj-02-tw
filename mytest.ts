
// change the path of books
// path of books_categories
// path of books_authors
// guard function

import { Book } from "./models";


export async function main() {
    const csv = require('csv-parser')
    const fs = require('fs')

    await fs.createReadStream('book_depository_data/temp/head1.csv')
        .pipe(csv())    //
        .on('data', async (data: any) => {
            const book:Book = {
                id: data[''],
                bestsellers_rank: data['bestsellers-rank'],
                description: data.description,
                dimension_x: data['dimension_x'],
                dimension_y: data['dimension_y'],
                dimension_z: data['dimension_z'],
                edition: data['edition'],
                edition_statement: data['edition_statement'],
                for_ages: data['for_ages'],
                format_id: data['format_id'],
                origin_id: data['id'],
                illustrations_note: data['illustrations_note'],
                image_checksum: data['image_checksum'],
                image_path: data['image_path'],
                image_url: data['image_url'],
                imprint: data['imprint'],
                isbn10: data['isbn10'],
                isbn13: data['isbn13'],
                language: data['lang'],
                publication_date: data['publication_date'],
                rating_avg: data['rating_avg'],
                rating_count: data['rating_count'],
                title: data['title'],
                url: data['url'],
                weight: data['weight']
            }
            console.log(book);

        }).on('end', function () {
            console.log('done');
        });
}

main();