# %%
import time
import csv
import re
from scrape_description import get_description

# %%
def get_descriptions_csv(urls):
    # erase old data
    open('../descriptions.csv', 'w', encoding="UTF8").close()

    # append new data into the csv
    with open('../descriptions.csv', 'a', encoding='UTF8') as f:
        writer = csv.writer(f)
        
        # write column names
        header = ['isbn', 'description']
        writer.writerow(header)

        # scrape and write descriptions
        for url in urls:
            description = get_description(url)
            isbn = re.findall(r'(\d+)$', url)[0]
            writer.writerow([isbn, description])
            time.sleep(0.05)

# %%
# test cases
if __name__ == "__main__":
    urls = ['/The-Design-of-Everyday-Things-Donald--Norman/9780465050659',
        '/The-Signal-and-the-Noise-Nate-Silver/9780141975658',
        '/Little-Snowflake-Igloo-Books/9781838528416',
        '/Music-Fun-101-Sue-Albrecht-Johnson/9780739052563']
    get_descriptions_csv(urls)
    
# %%
