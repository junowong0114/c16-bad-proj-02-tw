# %%
import pandas as pd

# %%
df = pd.read_csv('../processed_csvs/dataset_processed.csv')
df.info()

# %%
df.head(10)

# %%
df_raw = pd.read_csv('../raw_csvs/dataset.csv')
df_raw.info()

# %%
df_raw.head(5)

# %%

df.head(500000).to_csv('../temp/head.csv')
# %%

df.head(1).to_csv('../temp/head1.csv')
# %%
