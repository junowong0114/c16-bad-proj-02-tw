# %%
import pandas as pd
import csv
import re

# %%
# read categories csv
df_categories = pd.read_csv("../raw_csvs/categories.csv")

# %%
# group duplicate name together and generate new ids
df_categories["new_id"] = df_categories.groupby('category_name', sort=False).ngroup()

# %%
# create a dictionary with old ids as keys and new ids as values
categories_map = pd.Series(df_categories.new_id.values, index=df_categories.category_id).to_dict()

# %%
# old ids to new ids mapping function
def get_new_id(old_id):
    return categories_map[old_id]

def convert_categories_string_to_list(categories_string):
    ids = re.findall(r'\[(.*)\]', categories_string)[0].split(',')
    filtered_ids = list(filter(lambda x: (x != '' and x!= ' '), ids))

    return filtered_ids

def convert_old_ids_string_to_new_ids_list(old_ids_string):
    old_ids = convert_categories_string_to_list(old_ids_string)
    return [get_new_id(int(n)) for n in old_ids]

# %%
# read original dataset in chunks
CHUNK_SIZE = 10 ** 5
df_dataset = pd.read_csv('../raw_csvs/dataset.csv', chunksize=CHUNK_SIZE)

# %%
# convert old list of categories to new list with new id

# clear old file if it is existing
with open('../processed_csvs/dataset_processed.csv', 'w') as f:
    # f.write('authors,bestsellers-rank,categories,description,dimension-x,dimension-y,dimension-z,edition,edition-statement,for-ages,format,id,illustrations-note,image-checksum,image-path,image-url,imprint,index-date,isbn10,isbn13,lang,publication-date,publication-place,rating-avg,rating-count,title,url,weight\n')
    f.close()

# the actual conversion
for chunk in df_dataset:
    chunk["categories"] = chunk["categories"].apply(convert_old_ids_string_to_new_ids_list)
    chunk.to_csv('../processed_csvs/dataset_processed.csv', mode='a', index=False)

# %%
df_processed = pd.read_csv('../processed_csvs/dataset_processed.csv')

# %%
df_export = df_processed[df_processed["authors"] != "authors"]

# %%
df_export.info()

# %%
df_export.to_csv('../processed_csvs/dataset_processed.csv', mode='w', index=False)

# %%
