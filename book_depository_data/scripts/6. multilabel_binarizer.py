# %%
import pandas as pd
from sklearn.preprocessing import MultiLabelBinarizer
import matplotlib.pyplot as plt
import re

# %%
df = pd.read_csv('../processed_csvs/dataset_processed.csv')
df.info()

# %%
df_formats = pd.read_csv(r'../raw_csvs/formats.csv', names=['format', 'format_name'], skiprows=[0])
df_formats["format"] = pd.to_numeric(df_formats["format"])

# %%
df["format"] = df["format"].fillna(0).astype('int64')

# %%
df = pd.merge(df, df_formats, on='format', how="left")
df.info()

# %%
def convert_categories_string_to_list(categories_string):
    ids = re.findall(r'\[(.*)\]', categories_string)[0].split(',')
    filtered_ids = list(filter(lambda x: (x != '' and x!= ' '), ids))

    return [int(n) for n in filtered_ids]

# %%
# transform column categories from string to list of integer
df["categories"] = df["categories"].apply(convert_categories_string_to_list)

# %%
# convert categories column to list
categories = df["categories"].values.tolist()

# %%
# binarize the categories column data
mlb = MultiLabelBinarizer()
mlb_result = mlb.fit_transform(categories)

# %%
df_categories = pd.read_csv(r'../processed_csvs/categories_processed.csv')
categories_dict = pd.Series(df_categories.category_name.values, index=df_categories.category_id).to_dict()

# %%
# get classes list
classes = [categories_dict[id] for id in list(mlb.classes_)] 
# classes = list(mlb.classes_)

# %%
# combined binarized result with isbn13
df_transformed = pd.concat([df['isbn13'], df['format_name'], df['lang'], pd.DataFrame(mlb_result, columns=classes)], axis=1)
df_transformed.head(4)

# %%
df_transformed.to_csv('../processed_csvs/isbn_categories_binarized_full_name.csv', index=False, mode='w')

# %%
# visualize the data (data preparation)
counts = []
for i, cls in enumerate(classes):
    counts.append((cls, df_transformed[cls].sum()))

df_stats = pd.DataFrame(counts, columns=['category', 'frequency']).sort_values(by="frequency", ascending=False)

# %%
# visualize the data (actual visualization)
df_stats.iloc[0:20].plot(x='category', y='frequency', kind='barh', legend=False, grid=True, figsize=(20, 20))
plt.title("Number of books per category")
plt.gca().invert_yaxis()
plt.xlabel('frequency', fontsize=12)
plt.ylabel('category', fontsize=12)

# %%
# check no of rows without descriptions
df['description'].isna().sum()

# %%
