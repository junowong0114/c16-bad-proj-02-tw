# %%
import pandas as pd

# %%
# read categories csv
df = pd.read_csv("../raw_csvs/categories.csv")
df.info()

# %%
df["new_id"] = df.groupby('category_name', sort=False).ngroup()

# %%
df["new_id"].value_counts()

# %%
df.to_csv('../categories_with_old_and_new_ids.csv', index=False)

# %%
df_new = pd.DataFrame()

# %%
df_new["category_id"] = df["new_id"]
df_new["category_name"] = df["category_name"]
df_new.head(5)

# %%
df_new.drop_duplicates(subset=["category_id"], inplace=True, ignore_index=True)
df_new.info()

# %%
df_new.head(5)

# %%
df_new.to_csv('../processed_csvs/categories_processed.csv', index=False)

# %%
