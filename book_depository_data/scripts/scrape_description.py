# %%
import requests
from bs4 import BeautifulSoup
import re

# %%
def get_description(url):
    url = f"https://www.bookdepository.com{url}"

    payload = ""
    headers = {
        "cookie": "bd-session-id=258-5319944-5499313; bd-session-id-time=2082787201l; bd-session-token=c5%2BcYso6IMiLUt%2FCJs66AXtwdZItSb0aBJ6BpKe332RTId3r3qZfQ6hI9WKD03QsJI5FL%2BjNJV%2F3WUcR7%2BYg3QMxg%2BjZB%2BmuNVDYSlQ%2Fgzw%2BeFUhnWzN7BgjTu%2FwjiMGiTZ0jl0LaOflGLO5akDVTuIK1%2BP9xkYWnrFsDR1%2FVgNsqoUEddi3ZM%2F3VfjqRMlh; ENTITY_SESS_ID_UK=6ec9067485e3f5f1e8d7510e85ef1855; bd-ubid-main=257-8825308-0625916",
        "authority": "www.bookdepository.com",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36"
    }

    r = requests.request("GET", url, data=payload, headers=headers)

    soup = BeautifulSoup(r.text, 'html.parser')
    description_list = soup.select('.item-excerpt')
    
    if len(description_list):
        description = description_list[0].get_text()
        return prettify_description(description)
    else:
        return ''

# %%
def prettify_description(description):
    processed = re.sub(r'(\n\s*)', '\n', description)

    pattern = re.compile(r'^\n(.*)\nshow more', re.DOTALL)
    processed = re.findall(pattern, processed)[0]
    return processed

# %%
# with show more example
if __name__ == '__main__':
    description = get_description('/The-Signal-and-the-Noise-Nate-Silver/9780141975658')
    print(description)

# %%
# without show more example
if __name__ == '__main__':
    description2 = get_description('/Little-Snowflake-Igloo-Books/9781838528416')
    print(description2)

# %%
# no description example
if __name__ == '__main__':
    description3 = get_description('/Music-Fun-101-Sue-Albrecht-Johnson/9780739052563')
    print(description3)

# %%
