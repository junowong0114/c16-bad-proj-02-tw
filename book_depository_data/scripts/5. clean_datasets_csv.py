# %%
import pandas as pd
import re

# %%
# read datasets.csv into memory
df = pd.read_csv(r'../processed_csvs/dataset_processed.csv')

# %%
# read formats.csv into memory, re-format the dataframe
df_formats = pd.read_csv(r'../raw_csvs/formats.csv', names=['format', 'format_name'], skiprows=[0])
df_formats["format"] = pd.to_numeric(df_formats["format"])

# %%
df.info()

# %%
# preprocess columns for denormalization
df["format"] = df["format"].fillna(0).astype('int64')

# %%
# denormalize dataset
df_merged = pd.merge(df, df_formats, on='format', how="left")
df_merged.info()

# %%
# filter out unwanted rows

# regEXP for categories: Stationery | Child Stationery
regEXP = re.compile(r'394|2258')

df_mod = df_merged[(df_merged["lang"] == 'en')
    & (df_merged["categories"].str.contains(regEXP) == False)
    & (df_merged["description"].isna() == False)
    & ((df_merged["format_name"] == 'Bath')
    | (df_merged["format_name"] == 'Board')
    | (df_merged["format_name"] == 'Book')
    | (df_merged["format_name"] == 'Boxed')
    | (df_merged["format_name"] == 'CD')
    | (df_merged["format_name"] == 'DVD')
    | (df_merged["format_name"] == 'Hardback')
    | (df_merged["format_name"] == 'Leather')
    | (df_merged["format_name"] == 'Mixed')
    | (df_merged["format_name"] == 'Other')
    | (df_merged["format_name"] == 'Paperback')
    | (df_merged["format_name"] == 'Sheet')
    | (df_merged["format_name"] == 'Spiral'))
    ]
df_mod.info()

# %%
# save modified datasets to datasets_mod.csv
df_mod.to_csv('../processed_csvs/dataset_mod.csv', index=False)

# %%