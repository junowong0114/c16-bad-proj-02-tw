# %%
import pandas as pd
import glob
import os

# %%
PATH = r'C:\Users\Juno Wong\Documents\Tecky\c16-bad-proj-02-tw\book_depository_data\scraped_descriptions'

# %%
def get_key(fp):
    filename = os.path.splitext(os.path.basename(fp))[0]
    int_part = filename.split()[0][0:-1]
    return int(int_part)

# %%
all_descriptions = sorted(glob.glob(os.path.join(PATH, '*.csv')), key=get_key)

# %%
dfs = (pd.read_csv(f) for f in all_descriptions)
df_descriptions = pd.concat(dfs, axis=0, ignore_index=True, names=["isbn13", "description"])
df_descriptions = df_descriptions.rename(columns={"isbn": "isbn13"})

# %%
df_descriptions.info()

# %%
df_descriptions.to_csv(r'C:\Users\Juno Wong\Documents\Tecky\c16-bad-proj-02-tw\book_depository_data\processed_csvs\descriptions_combined.csv', index=False)

# %%
