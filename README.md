# c16-bad-proj-02-tw

## Project initialization
1. Clone the repo into your local machine
```
git clone git@gitlab.com:junowong0114/c16-bad-proj-02-tw.git
```

2. Install node modules
```yarn install```

3. create .env file into repo directory
```
PYTHON_INSTALL_LOC={YOUR_DIRECTORY_TO_PYTHON.EXE}

## eg
PYTHON_INSTALL_LOC="C:/Users/Juno Wong/miniconda3/python.exe"
```

4. create new conda environment for the project
```
conda create --name {environment_name} --requirements.txt
conda activate {environement_name}

## eg
conda create --name bad_proj --requirements.txt
```

5. Install python packages
```
pip install -r ./requirements.txt
```