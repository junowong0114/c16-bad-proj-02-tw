

import { Request, Response } from 'express';
import { BookService } from "../services/BookService";


export class BookController {
    constructor(private bookService: BookService) { };

    addBook = async (req: Request, res: Response) => {
        const title =  req.body.title + '';
        const author = req.body.author + '';
        const isbn = req.body.isbn + '';
        const description = req.body.description + '';
        const categories:any = req.body.categories;
        const image = req.body.image;
        const book = await this.bookService.addBook(title,author,isbn,description,categories,image);
        res.json(book)
    }

}

