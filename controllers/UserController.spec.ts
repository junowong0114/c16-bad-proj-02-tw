import { UserService } from "../services/UserService";
import { UserController } from "./UserController"
import { Request, Response } from "express";
import { Knex } from "knex";
// import { hashPassword } from "../hash";
// import { checkPassword } from "../hash";


describe('UserController', () => {
    let userController: UserController;
    let userService: UserService;
    let req: Request;
    let res: Response;

    beforeEach(() => {
        userService = new UserService({} as Knex);


        jest.spyOn(userService, 'getUser').mockImplementation(
            async () => Promise.resolve([{
                id:1, username:'admin@gmail.com', password:'a hash password'
            }])
        )
        // jest.spyOn(userService,'updatePassword').mockImplementation(async()=>{});
        

        userController = new UserController(userService);
        
        res = {
            json: jest.fn(() => null),
            redirect: jest.fn(()=>null),
            status:jest.fn(()=>({
                json: jest.fn(() => null),
                redirect: jest.fn(()=>null),
            }))
        } as any as Response;

    })


    // it('should login', async () => {
    //     req = {
    //         body: {
    //             username: 'admin@gmail.com',
    //             password: 'a hash password'
    //         }
    //     } as Request;

    //     const user = await userController.login(req, res);


    //     expect(userService.getUser).toBeCalledWith('admin@gmail.com')
    //     expect(res.status).toBeCalledTimes(1);
    //     expect(res.json).toBeCalledWith([{id:1, username:'admin@gmail.com', password:'a hash password'}])
    //     expect(user[0]).toMatchObject({id:1, username:'admin@gmail.com', password:'a hash password'});
    // })

    it('should logout',async()=>{
        req={
            session:{
                user:{
                    id:1,
                    username:'admin@gmail.com',
                    password:'password'
                }
            }
        } as any as Request;

        await userController.logout(req,res);

        expect(req.session['user']).toBeUndefined();
        expect(res.redirect).toHaveBeenCalledTimes(1);
    })

    // it('should update password',async()=>{
    //     req={
    //         body:{
    //             newPassword:'123',
    //             repeatPassword:'123'
    //         },
    //         session:{
    //             user:{
    //                 id:1,
    //                 username:'admin@gmail.com',
    //                 password:'password'
    //             }
    //         }
    //     } as any as Request;

    //     await userController.updatePassword(req,res);
    //     expect(hashPassword).toBeCalledWith('123');
    // })


})