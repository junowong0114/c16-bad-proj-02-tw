import { SearchService } from "../services/SearchService";
import { SearchController } from "./SearchController";
import { Request, Response } from "express";
import { Knex } from "knex";


describe('SearchController', () => {
    let searchController: SearchController;
    let searchService: SearchService;
    let req: Request;
    let res: Response;

    beforeEach(() => {
        searchService = new SearchService({} as Knex);
        searchController = new SearchController(searchService);
        res = {
            json: jest.fn(() => null)
        } as any as Response;

        jest.spyOn(searchService, 'searchBook').mockImplementation(
            async () => Promise.resolve([{
                id: 2037,
                title: 'brothers',
                isbn13: '9781093331721',
                image_path: 'full/b/a/e/baed0ce847544699a317ecb71ec0e8c3ff157b7f.jpg',
                image_url: 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9781/0933/9781093331721.jpg',
                authors_name: ['Ben'],
                categories_name: ['book']
            }])
        );
            
        jest.spyOn(searchService, 'getBook').mockImplementation(
            async () => Promise.resolve([{
                id: 10,
                title: 'Stranger Fillings : Edible recipes to turn your world upside down!',
                isbn13: '9781409173359',
                image_path: 'full/3/a/4/3a444f0d8cb2b4ac55938ea31b5f76f151b4b5a3.jpg',
                image_url: 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9781/4091/9781409173359.jpg',
                description: 'When a young boy goes missing, a small town uncovers a mystery involving secret recipes, supernatural whisking forces and one strange ingredient...',
                authors_name: ['The Muffin Brothers'],
                categories_name: ['General Cookery', 'Parodies & Spoofs', 'TV Tie-in Humour']
            }])
        )

    })


    it('should search books', async () => {
        req = {
            query: {
                keyword: 'brothers'
            }
        } as any as Request;

        await searchController.searchBooks(req, res);

        expect(searchService.searchBook).toBeCalledWith('brothers');
        expect(res.json).toBeCalledWith([{
            id: 2037,
            title: 'brothers',
            isbn13: '9781093331721',
            image_path: 'full/b/a/e/baed0ce847544699a317ecb71ec0e8c3ff157b7f.jpg',
            image_url: 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9781/0933/9781093331721.jpg',
            authors_name: ['Ben'],
            categories_name: ['book']
        }]);
    });


    it('should get a book information', async () => {
        req = {
            query: {
                id: 2037
            }
        } as any as Request;

        await searchController.getBook(req, res);

        expect(searchService.getBook).toBeCalledWith(2037);
        expect(res.json).toBeCalledWith([{
            id: 10,
            title: 'Stranger Fillings : Edible recipes to turn your world upside down!',
            isbn13: '9781409173359',
            image_path: 'full/3/a/4/3a444f0d8cb2b4ac55938ea31b5f76f151b4b5a3.jpg',
            image_url: 'https://d1w7fb2mkkr3kw.cloudfront.net/assets/images/book/lrg/9781/4091/9781409173359.jpg',
            description: 'When a young boy goes missing, a small town uncovers a mystery involving secret recipes, supernatural whisking forces and one strange ingredient...',
            authors_name: ['The Muffin Brothers'],
            categories_name: ['General Cookery', 'Parodies & Spoofs', 'TV Tie-in Humour']
        }])
    })




})