import { Request, Response } from 'express';
import { CategoriesService } from "../services/categoriesService";



export class CategoriesController {
    constructor(private categoriesService: CategoriesService) { };

    getCategories = async (req: Request, res: Response) => {
        const categories = await this.categoriesService.getCategories();
        res.json(categories);
    }
}

