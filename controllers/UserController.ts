import { UserService } from "../services/UserService";
import {Request,Response} from 'express';
import { User } from "../models";
import { checkPassword, hashPassword } from "../hash";


export class UserController{
    constructor(private userService:UserService){};

    login = async(req:Request,res:Response)=>{
        const {username,password} = req.body;
        let users:User[] = await this.userService.getUser(username);
        const user = users[0];
        if(user && await checkPassword(password,user.password)){
            req.session['user'] = user;
            res.status(200).json({success:true});
        }else{
            res.status(401).json({success:false,msg:"Username/Password is incorrect!"});
        }
    }

    logout = async(req:Request,res:Response)=>{
        delete req.session['user'];
        res.redirect('/');
    }

    updatePassword = async(req:Request,res:Response)=>{
        const {newPassword,repeatPassword} = req.body;
        const id = req.session['user'].id;

        if(newPassword===repeatPassword){
            const password = await hashPassword(newPassword);
            await this.userService.updatePassword(id,password);
            res.json({msg:'success'});
        }else{
            res.json({msg:'failure'})
        }
        
    }
}