import { Request, Response } from 'express';
import { SearchService } from "../services/SearchService";

// import fs from 'fs'
// import path from 'path';
// import nodeFetch from 'node-fetch'




export class SearchController {
    constructor(private searchService: SearchService) { };

    searchBooks = async (req: Request, res: Response) => {
        const keyword = req.query.keyword + '';
        const books = await this.searchService.searchBook(keyword);
        res.json(books)
    }

    getBook = async (req: Request, res: Response) => {
        const id = req.query.id + '';
        const book = await this.searchService.getBook(parseInt(id));
        res.json(book)
    }


    // searchBooksDownloadPictures = async (req: Request, res: Response) => {
    //     const keyword = req.query.keyword + '';
    //     const books = await this.searchService.searchBook(keyword);

    //     for (let book of books) {
    //         const folder = '/book_depository_data/'
    //         const image = path.join(folder, book.image_path);
    //         let url = book.image_url

    //         if (!fs.existsSync(image) && url) {
    //             // const basename = path.basename(image);
    //             const dirname = path.dirname(image)

    //             if (!fs.existsSync(dirname)) {
    //                 fs.mkdirSync(dirname, {
    //                     recursive: true
    //                 })
    //             }

    //             const response: any = await nodeFetch(url)
    //             const buffer = await response.buffer();
    //             fs.writeFileSync(image, buffer)
    //         } else {
    //             console.log('image exists', image)
    //         }
    //     }
    //     res.json(books)
    // }

}

