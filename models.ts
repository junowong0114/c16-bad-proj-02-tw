export interface Format{
    id: number;
    name: string;
}

export interface Category {
    id: number,
    name: string
}

export interface Author {
    id: number,
    name: string
}

export interface Book {
    id:number,
    bestsellers_rank:number,
    description:string,
    dimension_x:number,
    dimension_y:number,
    dimension_z:number,
    edition:string,
    edition_statement:string,
    for_ages:string,
    format_id:number,
    origin_id:number,
    illustrations_note:string,
    image_checksum:string,
    image_path:string,
    image_url:string,
    imprint:string,
    isbn10:string,
    isbn13:string,
    language:string,
    publication_date:string,
    rating_avg:number,
    rating_count:number,
    title:String,
    url:string,
    weight:number
}

export interface User{
    username:string;
    password:string
}