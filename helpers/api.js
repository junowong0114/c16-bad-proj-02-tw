export function getAPIServer() {
  if (process.env.NODE_ENV === "development") {
    return 'http://0.0.0.0:8110'
  }
  return 'https://book-depository.junowong.me'
}

export function getAIServer() {
  if (process.env.NODE_ENV === "development") {
    return 'http://0.0.0.0:8100'
  }
  return 'https://book-depository-ai.junowong.me'
}