import { Knex } from "knex";
import { Format } from "../models";

export async function seed(knex: Knex): Promise<void> {
    await knex("formats").del();

    const csv = require('csvtojson')
    const results: Format[] = await csv({
        noheader: false,
        headers: ['id', 'name']
    }).fromFile('./book_depository_data/raw_csvs/formats.csv');

    await knex.batchInsert('formats', results)
};
