import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    await knex("administrators").del();

    await knex("administrators").insert([
        { id: 1, username: "admin@gmail.com",password:"$2a$10$4Q0BpntO7g4cFY/h3wRykOiNw4gnuHkkkbKIXdFGUkNf.HsB7poU6" }, // password:123
    ]);

};
