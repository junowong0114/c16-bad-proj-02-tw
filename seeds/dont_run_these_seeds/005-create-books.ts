import { Knex } from "knex";
import { Book } from "../models";

export async function seed(knex: Knex): Promise<void> {
    await knex("books").del();

    const csv = require('csvtojson')

    // const results: Book[] = await csv({
    await csv({
        ignoreColumns: /(authors|categories|publication-place|index-date)/,
        ignoreEmpty: true,
        maxRowLength:65535,
        headers: ['id', 'authors', 'bestsellers_rank', 'categories', 'description', 'dimension_x', 'dimension_y', 'dimension_z',
            'edition', 'edition_statement', 'for_ages', 'format_id', 'origin_id', 'illustrations_note', 'image_checksum', 'image_path',
            'image_url', 'imprint', 'index-date', 'isbn10', 'isbn13', 'language', 'publication_date', 'publication-place',
            'rating_avg', 'rating_count', 'title', 'url', 'weight'],
        colParser: {
            'id':(item:string) =>{
                return parseInt(item);
            },
            'bestsellers_rank': (item: string) => {
                return Math.floor(parseInt(item))
            },
            'rating_count': (item: string) => {
                return Math.floor(parseInt(item))
            },
            'format_id': (item: string) => {
                return Math.floor(parseInt(item))
            },
            'rating_avg': (item: string) => {
                return parseFloat(item)
            },
            'weight': (item: string) => {
                return parseFloat(item)
            },
        },
    }).fromFile('book_depository_data/processed_csvs/dataset50.csv'
    ).subscribe(async (json: Book, lineNumber: number) => {
        await knex.insert(json).into('books')
    })

    
    // '/book_depository_data/temp/head.csv'
    // book_depository_data/raw_csvs/dataset.csv
    // await knex.batchInsert('books', results, 3000)

};
