import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    await knex("books_categories").del();

    const csv = require('csvtojson')

    const results = await csv({
        includeColumns: /(^id$|categories)/,
        ignoreEmpty: true,
        headers: ['id', 'authors', 'bestsellers_rank', 'categories', 'description', 'dimension_x', 'dimension_y', 'dimension_z',
            'edition', 'edition_statement', 'for_ages', 'format_id', 'origin_id', 'illustrations_note', 'image_checksum', 'image_path',
            'image_url', 'imprint', 'index-date', 'isbn10', 'isbn13', 'language', 'publication_date', 'publication-place',
            'rating_avg', 'rating_count', 'title', 'url', 'weight'],
        colParser: {
            'id': (item: string) => {
                return parseInt(item);
            },
            'categories': (item: string) => {
                if (item === '') {
                    return
                }
                let result = item.slice(1, -1).split(',').map(x => parseInt(x))
                return result
            }
        },
    }).fromFile('book_depository_data/processed_csvs/dataset50.csv');


    for(let product of results){
        let id = product['id']
        for(let category_id of product['categories']){
            if (isNaN(category_id)) {
                continue;
            }
            await knex('books_categories').insert({
                book_id:id,
                category_id:category_id
            })
        }
    }
};
