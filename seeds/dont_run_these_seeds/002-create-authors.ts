import { Knex } from "knex";
import { Author } from "../../models";

export async function seed(knex: Knex): Promise<void> {
    await knex("books_authors").del();
    await knex("books_categories").del();
    await knex("books").del();
    await knex("authors").del();


    const csv = require('csvtojson')
    const results: Author[] = await csv({
        noheader: false,
        headers: ['id', 'name']
    }).fromFile('./book_depository_data/raw_csvs/authors.csv');

    await knex.batchInsert('authors', results,2000)
};
