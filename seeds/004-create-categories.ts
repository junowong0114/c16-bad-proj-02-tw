import { Knex } from "knex";
import { Category } from "../models";

export async function seed(knex: Knex): Promise<void> {
    await knex("books_categories").del();

    const csv = require('csvtojson')
    const results: Category[] = await csv({
        noheader: false,
        headers: ['id', 'name']
    }).fromFile('./book_depository_data/processed_csvs/categories_processed.csv');

    await knex.batchInsert('categories', results,2000)
};
