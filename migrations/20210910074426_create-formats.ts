import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('formats');
    if (!hasTable) {
        await knex.schema.createTable('formats', (table) => {
            table.increments();
            table.string('name').notNullable();
            table.timestamps(false,true);
        })
    }
}
export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('formats');
}

