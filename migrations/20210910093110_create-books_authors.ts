import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('books_authors');
    if (!hasTable) {
        await knex.schema.createTable('books_authors', (table) => {
            table.increments();
            table.integer('book_id').unsigned();
            table.foreign('book_id').references('books.id');
            table.integer('author_id').unsigned();
            table.foreign('author_id').references('authors.id')
            table.timestamps(false,true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('books_authors');
}

