import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('administrators');
    if(!hasTable){
        await knex.schema.createTable('administrators',(table)=>{
            table.increments();
            table.string('username').notNullable();
            table.string('password').notNullable();
            table.timestamps(false,true);
        })
    }

}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('administrators');
}

