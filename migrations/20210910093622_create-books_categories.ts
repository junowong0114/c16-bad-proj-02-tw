import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('books_categories');
    if (!hasTable) {
        await knex.schema.createTable('books_categories', (table) => {
            table.increments();
            table.integer('book_id').unsigned();
            table.foreign('book_id').references('books.id');
            table.integer('category_id').unsigned();
            table.foreign('category_id').references('categories.id')
            table.timestamps(false,true);
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('books_categories');
}

