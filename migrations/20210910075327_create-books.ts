import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasTable = await knex.schema.hasTable('books');
    if(!hasTable){
        await knex.schema.createTable('books',(table)=>{
            table.increments();
            table.text('title');
            table.text('description');
            table.timestamps(false,true);
            table.text('isbn13');
            table.text('image_path');
            table.text('for_ages');
            table.integer('format_id').unsigned();
            table.foreign('format_id').references('formats.id');
            table.text('origin_id');
            table.text('language');
            table.integer('bestsellers_rank');
            table.float('dimension_x');
            table.float('dimension_y');
            table.float('dimension_z');
            table.text('edition');
            table.text('edition_statement');
            table.text('illustrations_note');
            table.text('image_checksum');
            table.text('image_url');
            table.text('imprint');
            table.string('isbn10');
            table.date('publication_date');
            table.float('rating_avg');
            table.integer('rating_count');
            table.text('url');
            table.float('weight');
        })
    }
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('books');
}

