import dotenv from 'dotenv';
dotenv.config();


module.exports = {

  // development environment
  development: {
    debug: true,
    client: "postgresql",
    connection: {
      database: process.env.DB_NAME,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD
    },
    pool: {
      min: 2,
      max: 10
      // propagateCreateError: false // <- default is true
    },
    // acquireConnectionTimeout: 1000000,
    migrations: {
      tableName: "knex_migrations"
    }
  },

  test: {
    client: 'postgresql',
    connection: {
      database: process.env.TESTDB_NAME,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  // staging environment
  staging: {
    client: "postgresql",
    connection: {
      database: process.env.DB_NAME,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD
    },
    pool: {
      min: 2,
      max: 10,
      propagateCreateError: false // <- default is true

    },
    acquireConnectionTimeout: 1000000,
    migrations: {
      tableName: "knex_migrations"
    }
  },

  // production environment
  production: {
    client: "postgresql",
    debug: false,
    connection: {
      host: "book-depository-db-server",
      database: process.env.DB_NAME,
      user: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  }

};