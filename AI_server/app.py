import os

from sanic import Sanic
from sanic.response import json, text
from sanic.exceptions import NotFound

import numpy as np
import pandas as pd

# import tensorflow as tf

from gensim.test.utils import get_tmpfile
from gensim.models.doc2vec import Doc2Vec

import nltk
from nltk import sent_tokenize
from nltk import word_tokenize
nltk.download('punkt')

from joblib import load

from sanic_cors import CORS

app = Sanic("Python Hosted Model")
CORS(app, automatic_options=True)

# neural network prediction model
# model = tf.saved_model.load('./model/')

# binary relevance classifier
# clf = load('./binary_relevance_model/br_model.joblib')
clf = load('./binary_relevance_model/br_model_50.joblib')

# get category name <-> category id dictionaries
categories_df = pd.read_csv('./categories/categories_processed.csv')

categories = pd.read_csv('./categories/categories_ordered.csv').head(50)['category'].tolist()
id_to_category_dict = dict(categories_df.to_numpy())
category_to_id_dict = {v: k for k, v in id_to_category_dict.items()}

# Doc2Vec model
f = get_tmpfile(os.path.abspath('./Doc2Vec_model/doc2vec_model_50'))
doc2vec_model = Doc2Vec.load(f)

@app.get("/")
def welcome(request):
    return text("Welcome to book-depository-ai server.")

@app.post("/")
async def callModel(request):
    content = request.json

    if "description" in content:
        description = content["description"]

        words = []
        sentences = sent_tokenize(description)
        for s in sentences:
            words.extend([w.lower() for w in word_tokenize(s)])
        description_vector = doc2vec_model.infer_vector(words)
        
        # raw_predictions = model([description_vector], training=False).numpy()[0]
        # predictions = list(map(lambda x: 1 if x > 0.5 else 0, raw_predictions))

        prediction = clf.predict([description_vector]).toarray()[0]

        predicted_categories_id = []
        predicted_categories = []
        for i, x in enumerate(prediction):
            if x == 1:
                predicted_categories_id.append(category_to_id_dict[categories[i]])
                predicted_categories.append(categories[i])
        return json({"success": True, "category_ids": predicted_categories_id, "categories": predicted_categories})
    else:
        return json({"success": False, "msg": "Description was not found in the request json body."})

@app.exception(NotFound)
async def notFound():
    return json({"success": False})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8100)