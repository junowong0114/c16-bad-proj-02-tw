const search_bar = document.getElementById("search-bar");

const debounce = (func, wait, immediate) => {
    let timeout;

    return function executedFunction() {
        let context = this;
        let args = arguments;

        let later = function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };

        let callNow = immediate && !timeout;

        clearTimeout(timeout);

        timeout = setTimeout(later, wait);

        if (callNow) func.apply(context, args);
    };
};



// search_bar.addEventListener('keypress', async (event) => {
//     if (event.key === 'Enter') {
//         search_bar.removeEventListener()
//         console.log('enter')
//         const value = document.querySelector('#search-bar').value;
//         if (value) {
//             const res = await fetch(`search?keyword=${value}`)
//             const results = await res.json();
//             loadSearching(results);
//         }
//     }
// })

search_bar.addEventListener('input', debounce(async (event) => {
    const value = document.querySelector('#search-bar').value;
    if (value) {
        const res = await fetch(`search?keyword=${value}`)
        const results = await res.json();
        loadSearching(results);
    }
}, 800));







function loadSearching(books) {
    let search_board = document.querySelector('#result-board')
    search_board.innerHTML = '';

    for (let book of books) {
        const authors = book.authors_name;
        const categories = book.categories_name;

        let authorsHTML = '';
        if (authors) {
            for (let author of authors) {
                authorsHTML += `<span class="author">${author}</span>`
            }
        }
        let categoriesHTML = '';
        if (categories) {
            for (let category of categories) {
                categoriesHTML += `<span class="category">${category}</span>`
            }
        }


        search_board.innerHTML += `
        <a href="/book_info?id=${book.id}" class="book-link">
        <div class="book">
            <div class="img-box">
                <img src="${book.image_url}" alt="img">
            </div>
            <div class="book-info">
                <div class="title">
                    <span class='my-title'>Title: </span> 
                    ${book.title}
                </div>
                <div class="authors">
                    <span class='my-title'>Author: </span>
                    ${authorsHTML}
                </div>
                <div class="categories">
                    <span class='my-title'>Category: </span>
                    ${categoriesHTML}
                </div>
                <div class="isbn">
                    <span class='my-title'>ISBN: </span>
                    ${book.isbn13}
                </div>
            </div>
        </div>
    </a>
        `



    }
}



