function getAIServer() {
  return 'https://book-depository-ai.junowong.me'
}

$(function () {
    $("#category").multipleSelect({
        filter: true
    })
})

categories = []

window.onload = async () => {
    const queries = new URLSearchParams(window.location.search);
    let isbn = queries.get('ISBN');
    if (isbn) {
        data = await fetch(`https://www.googleapis.com/books/v1/volumes?q=isbn:${isbn}`);
        let jsonData = await data.json();
        let bookData = jsonData['items'][0]['volumeInfo'];
        document.querySelector('#title').value = queries.get('title');
        document.querySelector('#author').value = queries.get('author');
        document.querySelector('#isbn').value = isbn;
        document.querySelector('#description').value = queries.get('description');
        if (bookData.imageLinks) {
            document.querySelector('#book-image').src = bookData.imageLinks[Object.keys(bookData.imageLinks)[0]];
        };

        let predictions = await fetch(getAIServer(), {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ "description": document.querySelector('#description').value })
        })
        let predictionsJson = await predictions.json();
        $("#category").multipleSelect('setSelects', predictionsJson.category_ids)

        categories = predictionsJson.categories

        document.querySelector('#categories_container').innerHTML = categories.join(', ')
    }
};

document.querySelector('#back-button')
    .addEventListener('click', () => {
        const queries = new URLSearchParams(window.location.search);
        window.location.href = `/book_info_new_book${queries.get('ISBN') ? '?ISBN=' + queries.get('ISBN') : ''}`
    });

document.querySelector('#add-button')
    .addEventListener('click', async (event) => {
        const formObject = {};

        formObject['title'] = document.querySelector('#title').value;
        formObject['author'] = document.querySelector('#author').value;
        formObject['isbn'] = document.querySelector('#isbn').value;
        formObject['description'] = document.querySelector('#description').value;
        formObject['image'] = document.querySelector('#book-image').src;
        const options = [];
        for (let option of $("#category").find(':selected')) {
            options.push(option.value)
        };
        formObject['categories'] = options;
        const res = await fetch('/book', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formObject)
        });
        window.location.href = `/scan-book`
    });

$('#category').multipleSelect({
    onClick: (view) => {
        if (view.checked) {
            categories.push(view.label)
        } else {
            categories = categories.filter((item) => {
                return (item != view.label)
            })
        }

        document.querySelector('#categories_container').innerHTML = categories.join(', ')
    }
})