const form = document.querySelector('#change-password')

form.onsubmit = async (target) => {
    target.preventDefault();

    const formObj = {
        newPassword: form.fname.value,
        repeatPassword: form.lname.value
    }

    if (formObj.newPassword && formObj.newPassword === formObj.repeatPassword) {
        const res = await fetch('/update-password', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formObj)
        });
        const result = await res.json();
        if(result.msg === 'success'){
            window.location.href = '/homepage'
        }else{
            document.querySelector('#message').innerText = "Passwords don't match"
        }

    } else {
        document.querySelector('#message').innerText = "Passwords don't match"
    }



}