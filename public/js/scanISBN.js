function onScanSuccess(decodedText, decodedResult) {
    document.querySelector('.code-number').value = decodedText;
}
var html5QrcodeScanner = new Html5QrcodeScanner(
    "qr-reader", { fps: 10, qrbox: 250 });
html5QrcodeScanner.render(onScanSuccess);

document.querySelector('.next-button')
    .addEventListener('click',async ()=>{
        if(document.querySelector('.code-number').value){
            window.location.href = `/book_info_new_book?ISBN=${document.querySelector('.code-number').value}`
        }else{
            window.location.href = `/book_info_new_book`
        }
    })