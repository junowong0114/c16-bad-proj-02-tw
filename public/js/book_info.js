
window.onload = async () => {
    let params = (new URL(document.location)).searchParams;
    let id = params.get('id');
    if(id){
        const res = await fetch(`/get_book?id=${id}`)
        const result = await res.json();
        const title = document.querySelector('#title');
        const authors = document.querySelector('#authors');
        const isbn = document.querySelector('#isbn')
        const categories = document.querySelector('#categories')
        const description = document.querySelector('#description')
        const book = result[0];

        const pictureTag = document.querySelector('#book-img')
        pictureTag.innerHTML =  `<img src="${book.image_url}" alt="a book image">`               


        title.value = book.title;
        isbn.value = book.isbn13;
        description.value = book.description;

        if(book.authors_name){
            authors.value = book.authors_name;
        }
        if(book.categories_name){
            categories.value = book.categories_name;
        }




    }
}


