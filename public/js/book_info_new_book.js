window.onload = async () => {
    const queries = new URLSearchParams(window.location.search);
    let isbn = queries.get('ISBN');
    if (isbn) {
        data = await fetch(`https://www.googleapis.com/books/v1/volumes?q=isbn:${isbn}`);
        let jsonData = await data.json();
        let bookData = jsonData['items'][0]['volumeInfo'];
        document.querySelector('#title').value = bookData.title;
        document.querySelector('#author').value = bookData.authors;
        document.querySelector('#isbn').value = isbn;
        document.querySelector('#description').value = bookData.description;
        if (bookData.imageLinks) {
            document.querySelector('#book-image').src = bookData.imageLinks[Object.keys(bookData.imageLinks)[0]];
        }
    }
};

document.querySelector('#back-button')
    .addEventListener('click', () => {
        window.location.href = `/scan-book`
    });

document.querySelector('#next-button')
    .addEventListener('click', () => {
        let title = document.querySelector('#title').value;
        let author = document.querySelector('#author').value;
        let isbn = document.querySelector('#isbn').value;
        let description = document.querySelector('#description').value;
        let imageLinks = document.querySelector('#book-image').src;
        window.location.href = `/loading-page?${title?'title='+title+'&':''}${author?'author='+author+'&':''}${isbn?'ISBN='+isbn+'&':''}${description?'description='+description+'&':''}${imageLinks!='http://localhost:8110/images/book.jpeg'?'imageLink='+imageLinks:''}`
    });