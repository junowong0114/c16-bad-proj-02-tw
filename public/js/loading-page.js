document.querySelector('#back-button')
    .addEventListener('click', () => {
        const queries = new URLSearchParams(window.location.search);
        window.location.href = `/book_info_new_book${queries.get('ISBN') ? '?ISBN=' + queries.get('ISBN') : ''}`
    })


setTimeout(() => {
    const queries = new URLSearchParams(window.location.search);
    window.location.href = `/category_page?${queries.get('title') ? 'title=' + queries.get('title') : ''}&${queries.get('author') ? 'author=' + queries.get('author') : ''}&${queries.get('ISBN') ? 'ISBN=' + queries.get('ISBN') : ''}&${queries.get('description') ? 'description=' + queries.get('description') : ''}`
}, 3000)