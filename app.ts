import express from 'express';
import expressSession from 'express-session';
import Knex from 'knex';
import { SearchService } from './services/SearchService';
import { SearchController } from './controllers/SearchController';
import { UserService } from './services/UserService';
import { UserController } from './controllers/UserController';
import { CategoriesService } from './services/categoriesService';
import { CategoriesController } from './controllers/categoriesController';
import { BookService } from './services/BookService';
import { BookController } from './controllers/BookController';



// import expressLayouts from 'express-ejs-layouts';

const knexConfigs = require('./knexfile');
const environment = process.env.NODE_ENV || 'development';
const knexConfig = knexConfigs[environment];
const knex = Knex(knexConfig);
//const cors = require('cors');


const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }))

app.use(expressSession({
  secret:"book depository",
  resave:true,
  saveUninitialized:true
}))

app.set('view engine', 'ejs');

app.use(express.static('public'));
app.use('/css', express.static(__dirname + 'public/css'));
app.use('/js', express.static(__dirname + 'public/js'));
app.use('/img', express.static(__dirname + 'public/images'));
//app.use(
//  cors({
//    origin: 'https://book-depository.xyz'
//  })
//)


// app.use(expressLayouts);


export const searchService = new SearchService(knex);
export const searchController = new SearchController(searchService);
export const userService = new UserService(knex);
export const userController = new UserController(userService);
export const categoriesService = new CategoriesService(knex);
export const categoriesController = new CategoriesController(categoriesService);
export const bookService = new BookService(knex);
export const bookController = new BookController(bookService);




app.use(express.static('./book_depository_data/full'));

app.post('/login',userController.login);
app.get('/logout',userController.logout);
app.post('/update-password',userController.updatePassword)
app.get('/searching',(req,res)=>{
  res.render('searching')
})

app.get('/book_info',(req,res)=>{
  res.render('book_info')
})

app.get('/get_book?',searchController.getBook)
app.get('/search',searchController.searchBooks)
app.post('/book',bookController.addBook)


app.get('/scan-book',(req,res)=>{
  res.render('scanISBN')
})

app.get('/account-setting',(req,res)=>{
  res.render('account-setting')
})

app.get('/loading-page',(req,res)=>{
  res.render('loading-page')
})

app.get('/book_info_new_book',(req,res)=>{
  res.render('book_info_new_book')
})

app.get('/homepage',(req,res)=>{
  res.render('homepage')
})

app.get('/category_page',(req,res)=>{
  res.render('category-page')
})

app.get('/categories',categoriesController.getCategories)




app.get('/',(req,res)=>{
  res.render('index')
})


const PORT = 8110;
app.listen(PORT, function () {
    console.log(`Listening at PORT ${PORT}.`)
});