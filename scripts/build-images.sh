#!/bin/bash
set -e
set -o pipefail
set -x

cd AI_server
docker build -t book-depository-ai-image .

cd ..
docker build -t book-depository-web-image .