#!/bin/bash
set -e
set -o pipefail
set -x

scripts/build-images.sh
scripts/aws/02-upload-image.sh
scripts/aws/03-deploy-image.sh