#!/bin/bash
set -e
set -o pipefail
set -x

scp scripts/etc/nginx/sites-available/default junowong.me:~/book-depository/default
ssh junowong.me "cd ~/book-depository"
ssh junowong.me "sudo chown root:root ~/book-depository/default"
ssh junowong.me "sudo mv ~/book-depository/default /etc/nginx/sites-available/default"
ssh junowong.me "sudo nginx -t"
ssh junowong.me "sudo service nginx restart"