#!/bin/bash
set -e
set -o pipefail
set -x

docker images | grep book-depository-web-image
docker save book-depository-web-image | zstd | ssh junowong.me "unzstd | docker load"

docker images | grep book-depository-ai-image
docker save book-depository-ai-image | zstd | ssh junowong.me "unzstd | docker load"