#!/bin/bash
set -e
set -o pipefail
set -x

scp junowong.me:/etc/nginx/sites-available/default scripts/etc/nginx/sites-available/default