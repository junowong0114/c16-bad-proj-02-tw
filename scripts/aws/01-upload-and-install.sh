#!/bin/bash
set -e
set -o pipefail
set -x

scp scripts/aws/install.sh junowong.me:~/install.sh
ssh junowong.me "~/install.sh"