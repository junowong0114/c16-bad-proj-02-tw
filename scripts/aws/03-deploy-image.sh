#!/bin/bash
set -e
set -o pipefail
set -x

scp docker-compose.yml junowong.me:~/book-depository

ssh junowong.me "cd ~/book-depository && docker-compose up -d"