#!/bin/bash
set -e
set -o pipefail
set -x

ssh junowong.me "sudo certbot --nginx"
ssh junowong.me "sudo nginx -t"
ssh junowong.me "sudo service nginx restart"
