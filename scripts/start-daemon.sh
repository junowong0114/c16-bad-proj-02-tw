#!/bin/bash
set -e
set -o pipefail
set -x

# docker run -d -it -p 8100:8100 book-depository-ai-image
docker-compose up
docker ps | awk '{print $2}'