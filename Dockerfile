FROM node:slim

WORKDIR /srv/book-depository-server

COPY . .
EXPOSE 8110
CMD yarn install && \
    yarn knex migrate:latest && \
    yarn knex seed:run && \
    node index.js
