-- get categories of the books
select books.id,title,categories.name AS category_name FROM books 
LEFT JOIN books_categories ON books.id = books_categories.book_id
LEFT JOIN categories ON books_categories.category_id = categories.id

-- get authors of the books
select books.id,title,authors.name AS author_name FROM books 
LEFT JOIN books_authors ON books.id = books_authors.book_id
LEFT JOIN authors ON books_authors.author_id = authors.id
WHERE title LIKE '%apple%' OR authors.name LIKE '%john%'
OR description LIKE '%apple%' 
ORDER BY books.id


select books.id,title, JSON_agg(categories.name) AS category_name FROM books 
LEFT JOIN books_categories ON books.id = books_categories.book_id
LEFT JOIN categories ON books_categories.category_id = categories.id
GROUP BY books.id,title


-- group by and json AGGREGATE
select books.id,title, JSON_agg(authors.name) AS authors_name FROM books 
LEFT JOIN books_authors ON books.id = books_authors.book_id
LEFT JOIN authors ON books_authors.author_id = authors.id
WHERE title LIKE '%brothers%' OR authors.name LIKE '%brothers%'
-- OR description LIKE '%brothers%' 
GROUP BY books.id,title


-- search books
select books.id,title,isbn13,image_path,image_url, JSON_agg(authors.name) AS authors_name ,Json_agg(categories.name) as categories_name 
FROM books 
LEFT JOIN books_authors ON books.id = books_authors.book_id
LEFT JOIN authors ON books_authors.author_id = authors.id
LEFT JOIN books_categories ON books.id = books_categories.book_id
LEFT JOIN categories ON books_categories.category_id = categories.id
WHERE title iLIKE '%brothers%' OR authors.name iLIKE '%brothers%'
OR description LIKE '%brothers%' 
GROUP BY books.id,title,isbn13,image_path,image_url


SELECT books.id,title,isbn13,image_path,image_url,description , JSON_agg(DISTINCT authors.name) AS authors_name 
    ,JSON_agg(DISTINCT categories.name) AS categories_name FROM books
LEFT JOIN books_authors ON books.id = books_authors.book_id
LEFT JOIN authors ON books_authors.author_id = authors.id
LEFT JOIN books_categories ON books.id = books_categories.book_id
LEFT JOIN categories ON books_categories.category_id = categories.id
WHERE books.id = 10
GROUP BY books.id,title,isbn13,image_path,image_url,description